import pygal
from pygal import Config
import numpy
import sys

## Usable Classes ##
class Sample:
    def __init__(self):
        pass

    Acceleration = None
    Gyro = None


class Acceleration:
    X = 0.0
    Y = 0.0
    Z = 0.0

    def __init__(self, x, y, z):
        self.X = self.format(x)
        self.Y = self.format(y)
        self.Z = self.format(z)

    def format(self, number):
        return number * 0.000732


class Gyro:
    X = 0.0
    Y = 0.0
    Z = 0.0

    def __init__(self, x, y, z):
        self.X = self.format(x)
        self.Y = self.format(y)
        self.Z = self.format(z)

    def format(self, number):
        return number * ((0.070 * 60) / 360)
## End Usable Classes ##

## Begin Script ##
inputFile = ""
byteLimit = 0

# Parse Agruments
for arg in sys.argv:
    if "input" in arg:
        inputFile = arg.replace("--input=", "")

    if "byte-limit" in arg:
        byteLimit = int(arg.replace("--byte-limit=", ""))

if inputFile == "":
    print("Missing input file.  Use '--input='")
    quit()

print("Processing file...")

bytes = []
samples = []

# Read bytes
with open(inputFile, "r") as f:
    tempBytes = numpy.fromfile(f, dtype=numpy.int16)
    if byteLimit == 0:
        bytes = tempBytes
    else:
        bytes = tempBytes[0:byteLimit]

for n in range(0, len(bytes), 6):
    tempSample = Sample()
    tempSample.Acceleration = Acceleration(bytes[n], bytes[n + 1], bytes[n + 2])
    tempSample.Gyro = Gyro(bytes[n + 3], bytes[n + 4], bytes[n + 5])
    samples.append(tempSample)

accelerationSamplesX = []
accelerationSamplesY = []
accelerationSamplesZ = []
gyroSamplesX = []
gyroSamplesY = []
gyroSamplesZ = []
gyroSamples = []

index = 0
for sample in samples:
    # print("===== Sample " + str(index) + " =====")
    # print("Accelerometer: x:" + sample.Acceleration.X + " y:" + sample.Acceleration.Y + " z:" + sample.Acceleration.Z)
    # print("Gyro: x:" + sample.Gyro.X + " y:" + sample.Gyro.Y + " z:" + sample.Gyro.Z)
    index += 1
    accelerationSamplesX.append({'value': sample.Acceleration.X, 'node': {'r': 1}})
    accelerationSamplesY.append({'value': sample.Acceleration.Y, 'node': {'r': 1}})
    accelerationSamplesZ.append({'value': sample.Acceleration.Z, 'node': {'r': 1}})
    gyroSamplesX.append({'value': sample.Gyro.X, 'node': {'r': 1}})
    gyroSamplesY.append({'value': sample.Gyro.Y, 'node': {'r': 1}})
    gyroSamplesZ.append({'value': sample.Gyro.Z, 'node': {'r': 1}})

fileName = inputFile.replace(".BIN", "")
fileName = inputFile.replace(".bin", "")

scatterFileName = fileName + '_Acceleration.svg'
gyroFileName = fileName + '_Gyro.svg'
config = Config()
config.xrange = (0, 200)

accelChar = pygal.Line(config)
accelChar.title = 'Acceleration'
accelChar.add('X', accelerationSamplesX)
accelChar.add('Y', accelerationSamplesY)
accelChar.add('Z', accelerationSamplesZ)
accelChar.render_to_file(scatterFileName)
print("Rendered: " + scatterFileName)

gyroChart = pygal.Line(config)
gyroChart.title = 'Gyro'
gyroChart.add('X', gyroSamplesX)
gyroChart.add('Y', gyroSamplesY)
gyroChart.add('Z', gyroSamplesZ)
gyroChart.render_to_file(gyroFileName)
print("Rendered: " + gyroFileName)
