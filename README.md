# How to Use

The script requires a --input flag for the relative path of the .BIN file and has an optional flag --byte-limit that can limit the number of bytes displayed (so use a multiple of 6).

Sample run with a log file in the same folder:


```
#!bash
# Returns two full SVG graphs
python TafGrapher.py --input=LOG6.bin

# Returns two partial SVG graphs with more detail
python TafGrapher.py --input=LOG6.bin --byte-limit=18000
```